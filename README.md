# Chat Playground

Website for preparing scenarios for Chat Bot

Inspired by:

- [codepen.io/ThomasDaubenton/pen/QMqaBN](https://codepen.io/ThomasDaubenton/pen/QMqaBN)
- [codepen.io/gustavoquinalha/pen/xmNNwP](https://codepen.io/gustavoquinalha/pen/xmNNwP)

![](docs/screenshot.png)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Brutal Mode

Double-click above the app to activate simplified `Brutal Mode`

![](docs/screenshot-2.png)