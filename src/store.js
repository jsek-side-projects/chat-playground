import Vue from 'vue'
import Vuex from 'vuex'
import firebase from 'firebase';

Vue.use(Vuex)

const config = {
  apiKey: "AIzaSyBOh-xt5iida9NITIfqcB15gjANduIOk54",
  authDomain: "chat-playground-0.firebaseapp.com",
  databaseURL: "https://chat-playground-0.firebaseio.com",
  projectId: "chat-playground-0",
  storageBucket: "chat-playground-0.appspot.com",
  messagingSenderId: "976238099323"
};

firebase.initializeApp(config)

export default new Vuex.Store({
  state: {
    contacts: [
      { name: 'Anonymous', photo: '', online: false },
      { name: 'Megan Leib', photo: 'https://image.noelshack.com/fichiers/2017/38/2/1505775062-1505606859-portrait-1961529-960-720.jpg', online: true },
      { name: 'John Doe', photo: 'http://e0.365dm.com/16/08/16-9/20/theirry-henry-sky-sports-pundit_3766131.jpg?20161212144602', online: false },
      { name: 'Dave Corlew', photo: 'http://thomasdaubenton.xyz/portfolio/images/photo.jpg', online: false },
      { name: 'Elen Olgrid', photo: 'http://www.boutique-uns.com/uns/185-home_01grid/polo-femme.jpg', online: true },
      { name: 'Paul Walker', photo: 'http://static.jbcgroup.com/news/pictures/cc70ae498569ecc11eaeff09224d4ba5.jpg', online: false },
    ],
    discussions: [],
    activeDiscussion: null,
  },
  mutations: {
    setDiscussions(state, discussions) {
      state.discussions = discussions
        .map(d => ({
          ...d,
          messages: d.messages || [],
          lastMessage: d.messages && d.messages.last().parts.last().text,
          lastInteraction: d.messages && d.messages.last().time,
        }));
    },
    switchDiscussion(state, id) {
      state.discussions.forEach(d => d.active = false);
      state.discussions[id].active = true;
      state.discussions = [...state.discussions]; // force update
      state.activeDiscussion = state.discussions[id]
    },
    addMessage(state, msg) {
      const activeDiscussionId = state.discussions.findIndex(x => x.active);
      const lastMessageId = state.activeDiscussion.messages
        ? state.activeDiscussion.messages.length - 1
        : -1;

      const lastMessage = state.activeDiscussion.messages && state.activeDiscussion.messages.last();
      if (lastMessage && lastMessage.incoming === msg.incoming) {
        firebase
          .database()
          .ref(`discussions/${activeDiscussionId}/messages/${lastMessageId}/parts/${lastMessage.parts.length}`)
          .set({ text: msg.text })

        firebase
          .database()
          .ref(`discussions/${activeDiscussionId}/messages/${lastMessageId}`)
          .update({ time: msg.time })

        lastMessage.parts.push({ text: msg.text });
        lastMessage.time = msg.time;
      } else {
        firebase
          .database()
          .ref(`discussions/${activeDiscussionId}/messages/${lastMessageId + 1}`)
          .set({
            incoming: msg.incoming,
            time: msg.time,
            parts: [{ text: msg.text }]
          })

        state.activeDiscussion.messages.push({
          incoming: msg.incoming,
          time: msg.time,
          parts: [{ text: msg.text }]
        });
      }
    },
    removeLastMessage(state) {
      const activeDiscussionId = state.discussions.findIndex(x => x.active);
      const lastMessageId = state.activeDiscussion.messages.length - 1;

      const lastMessage = state.activeDiscussion.messages.last();
      if (!lastMessage) return;
      if (lastMessage.parts.length > 1) {

        firebase
          .database()
          .ref(`discussions/${activeDiscussionId}/messages/${lastMessageId}/parts/${lastMessage.parts.length - 1}`)
          .remove()

        lastMessage.parts.pop();
      } else {
        firebase
          .database()
          .ref(`discussions/${activeDiscussionId}/messages/${lastMessageId}`)
          .remove()

        state.activeDiscussion.messages.pop();
      }
    },
    updateMessage(state, { selection, text }) {
      const activeDiscussionId = state.discussions.findIndex(x => x.active);

      const { messageIndex, partIndex } = selection;
      const target = state.activeDiscussion.messages[messageIndex].parts[partIndex];

      firebase
        .database()
        .ref(`discussions/${activeDiscussionId}/messages/${messageIndex}/parts/${partIndex}`)
        .update({ text: text })

      target.text = text;
      state.activeDiscussion.messages = [...state.activeDiscussion.messages]
    },
    setUser(state, user) {
      state.user = user;
    }
  },
  actions: {
    login({ commit, dispatch }) {

      firebase.auth()
        .onAuthStateChanged(user => {
          if (user) {
            commit('setUser', user);
            dispatch('loadDiscussions');
          } else {
            const provider = new firebase.auth.GoogleAuthProvider();
            firebase
              .auth()
              .signInWithRedirect(provider)
              .then(user => {
                commit('setUser', user);
                dispatch('loadDiscussions');
              })
              .catch(() => {
                commit('setUser', null);
              });
          }
        });

    },
    loadDiscussions({ commit }) {
      firebase
        .database()
        .ref('discussions')
        .once('value', snapshot => {
          const discussions = snapshot.val();
          commit('setDiscussions', discussions);
          if (discussions.length > 0) {
            commit('switchDiscussion', 0);
          }
        });
    }
  }
})
